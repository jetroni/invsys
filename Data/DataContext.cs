using Microsoft.EntityFrameworkCore;
using InvSys.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.Security.Cryptography;

namespace InvSys.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        private string HashString(string str)
        {
            var message = Encoding.Unicode.GetBytes(str);
            var hash = new SHA256Managed();

            var hashValue = hash.ComputeHash(message);
            return Encoding.Unicode.GetString(hashValue);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            modelBuilder.Entity<Room>().HasOne(c => c.Floor).WithMany(c => c.Rooms).IsRequired().HasForeignKey(c => c.FloorId);
            modelBuilder.Entity<ReportDetail>().HasOne(c => c.Report).WithMany(c => c.ReportDetails).IsRequired().HasForeignKey(c => c.ReportId);
            modelBuilder.Entity<ReportDetail>().HasOne(c => c.Item).WithMany(c => c.ReportDetails).IsRequired().HasForeignKey(c => c.ItemId);

            modelBuilder.Entity<User>().HasData(new User{ Id = 1, Email = "admin@admin.com", Username = "admin", Password = HashString("Administrator123#"), Role = "Administrator" });
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Floor> Floors { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<ReportDetail> ReportDetails { get; set; }
    }
}
