﻿using InvSys.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvSys.ViewModels
{
    public class IndexViewModel
    {
        public int users { get; set; }
        public int rooms { get; set; }
        public int reports { get; set; }
        public int items { get; set; }
        public int floors { get; set; }
    }
}
