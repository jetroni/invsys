﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InvSys.Models
{
    public class ReportDetail
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int ReportId { get; set; }

        [Required]
        public int ItemId { get; set; }


        public virtual Item Item { get; set; }
        public virtual Report Report { get; set; }
    }
}
