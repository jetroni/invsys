﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InvSys.Models
{
    public class Item
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public int RoomId { get; set; }

        public string Image { get; set; }

        public virtual Room Room { get; set; }

        public virtual ICollection<ReportDetail> ReportDetails { get; set; }
    }
}
