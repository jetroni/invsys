﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InvSys.Models
{
    public class Room
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int FloorId { get; set; }

        public virtual Floor Floor { get; set; }
        public virtual ICollection<Item> Items { get; set; }
    }
}
