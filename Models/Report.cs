﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InvSys.Models
{
    public class Report
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime Created { get; set; }


        public virtual ICollection<ReportDetail> ReportDetails { get; set; }
    }
}
